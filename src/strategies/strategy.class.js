export default class Strategy {

  constructor ( marketAccount, marketplace ) {
    this.marketAccount = marketAccount
    this.marketplace = marketplace
  }


  static defaultSettings = {

    // This should define the maximum ammount that could be used by the strategy since the first order
    CURRENCY_TOTAL_USAGE_LIMIT: 1, // percentage

    // This should define the maximum amount that could be used by the strategy at each period tick
    CURRENCY_PERIOD_USAGE_LIMIT: 1, // percentage

  }

  async setup () {
    const { settings, keys } = this.marketAccount
    this.settings = { ...this.contructor.defaultSettings, ...settings }
    if ( keys ) this._setup(keys)
  }
  async _setup ( keys ) {
    notImplemented.call(this,'_setup')
  }

  async run () {
    notImplemented.call(this,'run')
  }

}


function notImplemented ( method ) {
  throw new Error(`The method ${this.constructor.name}.${method} is not implemented yet!`)
}