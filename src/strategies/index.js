export * as strategies  from './strategies'

// do not cheange
import markets from '../static-data/markets'
import * as marketplaces from '../marketplaces'
export async function runStrategy ( marketAccount ) {

  if ( ! marketAccount ) throw new TypeError('marketAccount argument required')

  const { strategy, market } = marketAccount

  if ( ! markets[market] ) throw new Error('Invalid market id')
  if ( ! strategies[strategy] ) throw new Error('Invalid strategy id')

  const Marketplace = marketplaces[ markets[market].marketplace ]
  if ( ! Marketplace ) throw new Error('Invalid Marketplace')

  const marketplace = new Marketplace( marketAccount, markets[market] )
  await marketplace.setup()

  // run strategy
  return await strategies[strategy]( marketplace, marketAccount )
}