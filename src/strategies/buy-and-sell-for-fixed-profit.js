import { createProgression } from '../utils/trading'


export default class BuyLowSellForProfitStrategy extends Strategy {

  static defaultSettings = {

    // BUY
    FUNDS_USAGE_LIMIT_PERCENTAGE: 0.3,
    BUY_OFFSET_PERCENTAGE: 0.05, // this will avoid us to place buy orders that gets taker matched
    BUY_RANGE_LIMIT_PERCENTAGE: 0.3,
    BUY_ORDERS_LIMIT: 10,

    // SELL
    SELL_PROFIT_PERCENTAGE: 0.05,
    SELL_OFFSET_PERCENTAGE: 0.05, // this will avoid us to place sell orders that gets taker matched

  }

  async run () {
    const {
      marketplace,
      settings: {

      }
    } = this

    // # Check for all BROUGHT orders and create SELL orders and the trade

    // # Check for all sold orders and update each trade

    // # Remove all BUY OPEN orders from GDAX

    // # Generate NEW BUY ORDERS on GDAX

  }
}


function createProgression ( value, amount = 10, limit = 0.1 ) {

  if ( ! value ) {
    throw new TypeError('value is required')
  }

  return calculateProgression(amount, percentage)
  .map( progression => value * progression )
}

function calculateProgression ( amount = 10, limit = 0.1 ) {

}