import { MAILER_SUBJECT_PREFIX, MAILER_SIGNATURE_TEXT } from '../config'

export default ({ user, recoverAuth }) => ({
  to: user.emails.map(email => email.address),
  subject: `${MAILER_SUBJECT_PREFIX} - ${user.name.first}, your password has been changed!`,
  text: `
  Dear ${user.name.first} ${user.name.last},

  We just wanted to let you know that your password has been changed successfully!
  Please let us know if the password change has not been requested by you.

  ${MAILER_SIGNATURE_TEXT}
`
})
