import { MAILER_SUBJECT_PREFIX, MAILER_SIGNATURE_TEXT } from '../config'

export default ({ user, recoverAuth }) => ({
  to: user.emails.map(email => email.address),
  subject: `${MAILER_SUBJECT_PREFIX} - ${user.name.first}, did you lost your account's password?`,
  text: `
  Dear ${user.name.first} ${user.name.last},

  We have received a request from someone to recover your account password.
  If this wasn't you, please ignore this email and your account will be safe!

  Please enter the authentication code below on your mobile app or website:

  ${recoverAuth.secret}

  ${MAILER_SIGNATURE_TEXT}
`
})
