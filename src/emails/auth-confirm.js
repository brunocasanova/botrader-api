import { MAILER_SUBJECT_PREFIX, MAILER_SIGNATURE_TEXT } from '../config'

export default ({ user, confirmAuth }) => ({
  to: user.emails.map(email => email.address),
  subject: `${MAILER_SUBJECT_PREFIX} - ${user.name.first}, please confirm your email address!`,
  text: `
  Dear ${user.name.first} ${user.name.last},

  We need to confirm that you own this email.
  If this wasn't you, please ignore this email!

  Please enter the authentication code below on your mobile app or website:

  ${confirmAuth.secret}

  ${MAILER_SIGNATURE_TEXT}
`
})
