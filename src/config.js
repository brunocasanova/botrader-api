const { env } = process

export const ENV = env.NODE_ENV || 'development'
export const NODE_ENV = ENV
export const ENV_TEST = ENV === 'test'
export const ENV_DEV = ENV === 'development'
export const ENV_PROD = ENV === 'production'

export const BRAND_NAME = 'Example'
export const BRAND_DOMAIN = 'example.com'

export const API_PORT = env.API_PORT || (ENV === 'test' ? 10000 + Math.floor(Math.random()*5000) : 8080 )
export const BACKEND_URL = ENV === 'production' ? `https://api.${BRAND_DOMAIN}` : `http://localhost:${API_PORT}`
export const FRONTEND_URL = ENV === 'production' ? `https://app.${BRAND_DOMAIN}` : 'http://localhost:3000'
export const API_CORS = { origin: FRONTEND_URL }
export const API_MORGAN = ! ENV_TEST
export const API_HELMET = true

export const DB_HOST = env.DB_HOST || '127.0.0.1'
export const DB_PORT = env.DB_PORT || '27017'
export const DB_USERNAME = env.DB_USERNAME || ''
export const DB_PASSWORD = env.DB_PASSWORD || ''
export const DB_NAME = env.DB_NAME || 'example' + (ENV === 'test' ? '-test' : '')
export const DB_URI = env.DB_URI || `mongodb://${DB_USERNAME && `${DB_USERNAME}${DB_PASSWORD && `:${DB_PASSWORD}` || ''}@` || ''}${DB_HOST}:${DB_PORT}/${DB_NAME}`

export const JWT_SECRET = env.JWT_SECRET || 'CHANGEME'
export const JWT_ALGORITHM = env.JWT_ALGORITHM || 'HS256'
export const JWT_ACCESS_VALIDITY = env.JWT_ACCESS_VALIDITY || '7d'
export const JWT_REFRESH_VALIDITY = env.JWT_REFRESH_VALIDITY || '30d'

export const AUTH_RECOVER_TOKEN_TTL = 60 * 60 * 1000 // 1 hour

export const MAILER_SUBJECT_PREFIX = env.MAILER_SUBJECT_PREFIX || `${BRAND_NAME} - `
export const MAILER_SIGNATURE_TEXT = env.MAILER_SIGNATURE_TEXT || `
  --
  Cheers,
  ${BRAND_NAME} team
`

export const MAILER_FROM = env.MAILER_FROM || `example <no-reply@${BRAND_DOMAIN}>`
export const MAILER_REPLY_TO = env.MAILER_REPLY_TO || `example support team <support@${BRAND_DOMAIN}>`

export const MAILER_SES_ENDPOINT = env.MAILER_SES_ENDPOINT || 'https://email.eu-west-1.amazonaws.com'
export const MAILER_SES_ACCESS = env.MAILER_SES_ACCESS || ''
export const MAILER_SES_SECRET = env.MAILER_SES_SECRET || ''

export const MAILER_SMTP_TLS = env.MAILER_SMTP_TLS || true
export const MAILER_SMTP_HOST = env.MAILER_SMTP_HOST || 'smtp.gmail.com'
export const MAILER_SMTP_PORT = env.MAILER_SMTP_PORT || 587
export const MAILER_SMTP_USERNAME = env.MAILER_SMTP_USERNAME || ''
export const MAILER_SMTP_PASSWORD = env.MAILER_SMTP_PASSWORD || ''

export const MAILER_X_MAILER = env.MAILER_X_MAILER || `${BRAND_NAME} API - ${BRAND_DOMAIN}`

export const GDAX_API_ENDPOINT = env.GDAX_API_ENDPOINT || (ENV === 'production' ? 'https://api-public.gdax.com' : 'https://api-public.sandbox.gdax.com')
export const GDAX_WEBSOCKET_ENDPOINT = env.GDAX_WEBSOCKET_ENDPOINT || (ENV === 'production' ? 'https://ws-feed-public.gdax.com' : 'https://ws-feed-public.sandbox.gdax.com')
