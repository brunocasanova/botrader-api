
const SIDES = [ 'buy', 'sell' ]
const STATUSES = [ 'opened', 'partial', 'filled', 'cancelled' ]

export default class Marketplace {

  constructor ( marketAccount, market ) {
    this.marketAccount = marketAccount
    this.market = market
  }

  static defaultSettings = {
    // ...
  }

  async setup () {
    const { settings, keys } = this.marketAccount
    this.settings = { ...this.contructor.defaultSettings, ...settings }
    if ( keys ) this._setup(keys)
  }
  async _setup ( keys ) {
    notImplemented.call(this,'_setup')
  }

  async getCurrentPrice () {
    notImplemented.call(this,'getCurrentPrice')
  }

  async getOrder ( order_id ) {
    if ( ! order_id ) {
      throw new TypeError('Invalid order_id')
    }

    await this._log('CANCEL_ORDER', `Cancelling order with id ${order_id}` )

    return await this._getOrder( order_id )
  }
  async _getOrder () {
    notImplemented.call(this,'getOrder')
  }

  async getOrders ({ side, status } = {}) {

    if ( side && SIDES.indexOf(side) === -1 ) {
      throw new TypeError('Invalid side')
    }

    if ( status && STATUSES.indexOf(status) === -1 ) {
      throw new TypeError('You must define either opened or closed or none for all')
    }

    const { target, using } = market
    await this._log('CANCEL_ORDERS',
      ! side && ! status && (
        'Getting all orders'
      ) ||
      side && ! status && (
        `Getting all ${side} orders`
      ) ||
      ! side && status && (
        `Getting all ${status} orders`
      ) ||
      side && status && (
        `Getting all ${side} ${status} orders`
      )
    )

    return await this._getOrders({ side, status })
  }
  async _getOrders () {
    notImplemented.call(this,'getAllOrders')
  }

  async cancelOrder ( order_id ) {
    if ( ! order_id ) {
      throw new TypeError('Invalid order_id')
    }

    await this._log('CANCEL_ORDER', `Cancelling order with id ${order_id}` )

    return await this._cancelOrder( order_id )
  }
  async _cancelOrder ( order_id ) {
    notImplemented.call(this,'cancelOpenOrder')
  }
  async cancelOrders ({ side, status } = {}) {

    if ( side && SIDES.indexOf(side) === -1 ) {
      throw new TypeError('Invalid side')
    }

    if ( status && STATUSES.indexOf(status) === -1 ) {
      throw new TypeError('You must define either opened or closed or none for all')
    }

    const { target, using } = market
    await this._log('CANCEL_ORDERS',
      ! side && ! status && (
        'Cancelling all orders'
      ) ||
      side && ! status && (
        `Cancelling all ${side} orders`
      ) ||
      ! side && status && (
        `Cancelling all ${status} orders`
      ) ||
      side && status && (
        `Cancelling all ${side} ${status} orders`
      )
    )

    return await this._cancelOrders({ side, status })
  }
  async _cancelOrders () {
    notImplemented.call(this,'_cancelOrders')
  }


  async placeOrder ({ side, price, value }) {

    if ( side && SIDES.indexOf(side) === -1 ) {
      throw new TypeError('Invalid side')
    }

    if ( typeof price != 'number' || ! price || isNaN(price) ) {
      throw new TypeError('Invalid price')
    }

    if ( typeof value != 'number' || ! value || isNaN(value) ) {
      throw new TypeError('Invalid value')
    }

    const { target, using } = market
    this._log('PLACE_ORDER', `Placing ${side} order of ${value} ${target} for ${price} ${using}`)

    return await this._placeOrder({ side, price, value })
  }
  async _placeOrder () {
    notImplemented.call(this,'_placeOrder')
  }

}

function notImplemented ( method ) {
  throw new Error(`The method ${this.constructor.name}.${method} is not implemented yet!`)
}