import { Trade } from '../models'
import Gdax from 'gdax'
import { GDAX_API_ENDPOINT } from '../config'
import Marketplace from './marketplace.class'
import markets from '../static-data/markets'

const GDAX_MARKETS = markets.filter(({marketplace}) => marketplace == 'GDAX').map(({trading}) => trading.toUpperCase())
const publicClient = new Gdax.PublicClient()

// Will host an object { price, last_updated } for each gdax market
const MARKET_PRICES = {}

export default class GdaxMarketplace extends Marketplace {

  static markets = [
    {
      id: 'gdax-btc-usd',
      marketplace: 'GDAX',
      trading: 'btc-usd',
      currency: 'usd',
      asset: 'btc',
      min: 0.01,
    },
    {
      id: 'gdax-btc-eur',
      marketplace: 'GDAX',
      trading: 'btc-eur',
      currency: 'eur',
      asset: 'btc',
      min: 0.01,
    },
    {
      id: 'gdax-btc-gbp',
      marketplace: 'GDAX',
      trading: 'btc-gbp',
      currency: 'gbp',
      asset: 'btc',
      min: 0.01,
    },
    {
      id: 'gdax-eth-btc',
      marketplace: 'GDAX',
      trading: 'eth-btc',
      currency: 'btc',
      asset: 'eth',
      min: 0.01,
    },
    {
      id: 'gdax-eth-usd',
      marketplace: 'GDAX',
      trading: 'eth-usd',
      currency: 'usd',
      asset: 'eth',
      min: 0.01,
    },
    {
      id: 'gdax-eth-eur',
      marketplace: 'GDAX',
      trading: 'eth-eur',
      currency: 'eur',
      asset: 'eth',
      min: 0.01,
    },
    {
      id: 'gdax-ltc-btc',
      marketplace: 'GDAX',
      trading: 'ltc-btc',
      currency: 'btc',
      asset: 'ltc',
      min: 0.01,
    },
    {
      id: 'gdax-ltc-usd',
      marketplace: 'GDAX',
      trading: 'ltc-usd',
      currency: 'usd',
      asset: 'ltc',
      min: 0.01,
    },
    {
      id: 'gdax-ltc-eur',
      marketplace: 'GDAX',
      trading: 'ltc-eur',
      currency: 'eur',
      asset: 'ltc',
      min: 0.01,
    }
  ]

  async _setup ({ api, secret, passphrase }) {
    this.client = new Gdax.AuthenticatedClient(api, secret, passphrase, GDAX_API_ENDPOINT)
  }

  getCurrentPrice () {
    return MARKET_PRICES[this.market.trading.toUpperCase()].price
  }
}

// Utils
//////////////


// HANDLE
const websocket = new Gdax.WebsocketClient(GDAX_MARKETS, null /* GDAX_API_ENDPOINT */, null, { heartbeat: true })
websocket.on('message', (order) => {
  const { type, reason, product_id, price } = order

  if ( type === 'done' && reason === 'filled' && price ) {
    updateMarketPrice( product_id, price )
  }
})

// setup heartbeat and an array with current prices
// GDAX_MARKETS.reduce((a,b) => typeof a == 'object' ? {...a, [b]: { id: b } } : { [a]: { id: a }, [b]: { id: b } }, {})
function updateMarketPrice ( id, price ) {
  const MARKET = MARKET_PRICES[id] || {}

  MARKET.price = price
  MARKET.last_update = Date.now()
}