import NodeMailer from 'nodemailer'
import {
  MAILER_FROM,
  MAILER_REPLY_TO,
  MAILER_SES_ENDPOINT,
  MAILER_SES_ACCESS,
  MAILER_SES_SECRET,
  MAILER_SMTP_TLS,
  MAILER_SMTP_HOST,
  MAILER_SMTP_PORT,
  MAILER_SMTP_USERNAME,
  MAILER_SMTP_PASSWORD,
  MAILER_X_MAILER
} from './config'

const DEFAULT_SEND_OPTIONS = {
  from: MAILER_FROM,
  replyTo: MAILER_REPLY_TO,
}

export const ses = NodeMailer.createTransport({
  transport: 'ses',

  ServiceUrl: MAILER_SES_ENDPOINT,
  accessKeyId: MAILER_SES_ACCESS,
  secretAccessKey: MAILER_SES_SECRET,

  xMailer: MAILER_X_MAILER,
})

export const smtp = NodeMailer.createTransport({
  host: MAILER_SMTP_HOST,
  port: MAILER_SMTP_PORT, requireTLS: MAILER_SMTP_TLS,
  auth: {
    user: MAILER_SMTP_USERNAME,
    pass: MAILER_SMTP_PASSWORD
  },

  xMailer: MAILER_X_MAILER,
})

const sendMailGenerator = ( transport ) => (
  (options) => (
    transport.sendMail({ ...DEFAULT_SEND_OPTIONS, ...options })
  )
)

export const sendMailSES = sendMailGenerator( ses )
export const sendMailSMTP = sendMailGenerator( smtp )
