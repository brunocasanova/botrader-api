import { createLogger, format, transports } from 'winston'
const { combine, timestamp, printf } = format

const logger = createLogger({
  level: 'info',
  // transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log`
    // - Write all logs error (and below) to `error.log`.
    //
    // new winston.transports.File({ filename: 'error.log', level: 'error' }),
    // new winston.transports.File({ filename: 'combined.log' })
  // ]
})

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
  logger.add(new transports.Console({
    format: combine(
      timestamp(),
      printf(({ timestamp, label, level, message }) => {
        return `${new Date(timestamp).toLocaleTimeString()} ${label && label +' ' || ''}${level}: ${message}`
      })
    )
  }))
}

export default logger