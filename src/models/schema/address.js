import mongoose from 'mongoose'

const Address = new mongoose.Schema({
  street: String,
  city: String,
  state: String,
  county: String,
  zip: Number,
  zip_4: Number
})

export default Address