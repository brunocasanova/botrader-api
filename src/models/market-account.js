import { createModel, Types } from '../database'
import mongoose from 'mongoose'
import markets from '../static-data/markets'
import * as strategies from '../strategies'

export const { MarketAccount, Schema } = createModel('MarketAccount', {

  user_id: {
    type: Types.ObjectId,
    ref: 'User',
    required: true
  },

  market: {
    type: String,
    enum: {
      values: Object.keys( markets ),
      message: 'not a valid market',
    }
  },


  keys: {
    api: String, // public key
    secret: String, // private key
    passphrase: String, // optional password
  },

  strategy: {
    type: String,
    enum: {
      values: Object.keys( strategies ),
      message: 'not a valid strategy',
    }
  },

  marketSettings: {
    type: Object,
    default: {}
  },

  strategySettings: {
    type: Object,
    default: {}
  },

})

export default MarketAccount