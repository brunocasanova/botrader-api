import { createModel, Types } from '../database'

export const { Auth, Schema } = createModel('Auth', {

  // NOTE:
  // This attribute will allow multiple types of authentication attached to
  // the same user. We just have to strict under the service how much of which
  // type we want. Example: we can request two passwords for entry, or have
  // multiple social accounts linked (even of the same provider) with the user.
  type: {
    type: String,
    enum: [
      'password',
      'recover-token',

      // Social auth
      'facebook-token',
      'google-token',

      // Trading platforms
      'gdax-token',
    ],
    default: 'password'
  },

  // NOTE: secret could be an oAuth token, a salt-hashed password, etc.
  // Depending on the authentication type.
  secret: String,

  // Data object should store extra auth info
  data: {
    type: Object,
    default: {},
  },

  created_at: {
    type: Date,
    default: () => new Date()
  },

  // Allow authentication to expire
  expires_at: {
    type: Date,
    default: null
  },

  user_id: {
    type: Types.ObjectId,
    ref: 'User'
  }
})

export default Auth
