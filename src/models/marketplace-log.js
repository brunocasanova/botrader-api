import { createModel, Types } from '../database'
import mongoose from 'mongoose'
import logStatuses from '../static-data/log-statuses'

export const { MarketplaceLog, Schema } = createModel('MarketplaceLog', {

  market_account_id: {
    type: Types.ObjectId,
    ref: 'MarketAccount',
    required: true
  },

  action: {
    type: String,
    enum: {
      values: logStatuses,
      message: 'not a valid status'
    },
  },

  message: String,

  created_at: {
    type: Date,
    default: Date.now,
  }
})


export default Trade
