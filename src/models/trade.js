import { createModel, Types } from '../database'
import mongoose from 'mongoose'

const OrderDetails = new mongoose.Schema({
  size: Number, // amount brought / to sell / sold
  price: Number, // brought at price
  filled: Number,
  order_id: {
    type: String,
    unique: true,
  },
  created_at: Date,
  closed_at: Date,
  status: {
    type: String,
    enum: {
      values: [ 'open', 'done-cancelled', 'done-filled' ],
      message: 'not a valid status'
    },
    default: 'open',
  }
})

export const { Trade, Schema } = createModel('Trade', {

  market_account_id: {
    type: Types.ObjectId,
    ref: 'MarketAccount',
    required: true
  },

  status: {
    type: String,
    enum: {
      values: [ 'buying', 'brought', 'filling', 'cancelled', 'filled' ],
      message: 'not a valid status'
    },
    default: 'open',
  },

  closed: {
    type: Boolean,
    default: false,
  },

  // Resume values
  buy: OrderDetails,
  sells: [ OrderDetails ],

  earnings: {
    prospected: { type: Number, default: 0 },
    achieved: { type: Number, default: 0 },
  },

  created_at: {
    type: Date,
    default: Date.now,
  }
})


export default Trade
