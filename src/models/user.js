import { createModel, Types } from '../database'
import mongoose from 'mongoose'
import Address from './schema/address'
import genders from '../static-data/genders'
import roles from '../static-data/roles'

export const { User, Schema } = createModel('User', {

  // Personal info
  name: {
    first: String,
    last: String
  },
  dob: { type: Date, default: null },
  gender: {
    type: String,
    enum: {
      values: [ ...genders, null ],
      message: 'not a valid gender'
    }
  },

  // Contacts
  address: Address,
  phones: [
    {
      number: String,
      verified: { type: Date, default: null }
    }
  ],
  emails: [
    {
      // TODO: Add a test trying to register two users with the same email, the
      // second one must fail at the register endpoint.
      address: { type: String, unique: true, index: true },
      verified: { type: Date, default: null }
    }
  ],

  roles: {
    type: [{ type: String, enum: roles }],
    default: [],
    validate: (values) => {
      for ( let x in values ) {
        // check if role wasnt set earlier
        for ( let y = 0; y < x; y++ ) {
          if ( values[x] === values[y] ) {
            throw new Error('Role is duplicated and should only be stated once')
          }
        }
      }
    }
  },

})

User.findByEmail = async email => {
  email = typeof email == 'string' && email.toLowerCase() || email
  return (await User.findOne({ 'emails.address': email })) || null
}

export default User
