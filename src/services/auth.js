import EmailValidator from 'email-validator'
import { Auth, User } from '../models'
import { sendMailSMTP } from '../mailer'
import Service from '../utils/service-builder'
import { generateTokens } from '../utils/json-web-token'
import HttpError from '../utils/http-error'
import uuid from 'uuid/v4'
import { authenticatePassword, hashPassword } from '../utils/password'
import { ENV_TEST, AUTH_RECOVER_TOKEN_TTL } from '../config'
import roles from '../static-data/roles'

import authRecoveryEmail from '../emails/auth-recovery'
import authPasswordChangedEmail from '../emails/auth-password-changed'

export const AuthService = new Service({
  model: Auth,
  endpoint: '/auth',

  custom: {
    'POST /register': async function(req, res) {
      let {
        body: {
          email,
          password,

          first_name,
          last_name,
          dob
        }
      } = req

      // Pre validation
      ////

      if (!email || !EmailValidator.validate(email)) {
        throw new HttpError(400, 'Invalid email address')
      }

      if (!password || typeof password != 'string' || !password.trim()) {
        throw new HttpError(400, 'Empty password')
      } else {
        password = password.trim()
      }

      if (!first_name || typeof first_name != 'string' || !first_name.trim()) {
        throw new HttpError(400, 'Invalid first name')
      }
      if (!last_name || typeof last_name != 'string' || !last_name.trim()) {
        throw new HttpError(400, 'Invalid last name')
      }

      // Check for duplicates
      ////

      if (await User.findByEmail(email)) {
        throw new HttpError(409, 'Email already in use')
      }

      // We can create the user and authentication by now
      ////

      if (!client_id) {
        client_id = uuid()
      }

      // create user
      const user = new User({
        name: {
          first: first_name,
          last: last_name
        },
        // ssn,
        dob,
        emails: [{ address: email, verified: null }]
      })

      try {
        await user.save()
      } catch (caught_err) {
        throw new HttpError(500, 'Error creating user', caught_err)
      }

      try {
        // create auth
        const auth = new Auth({
          type: 'password',
          secret: hashPassword(password),
          user_id: user.id
        })

        await auth.save()
      } catch (caught_err) {
        // Delete user before responding
        await user.remove()

        throw new HttpError(500, 'Error creating authentication', caught_err)
      }

      // Seems everything went well til here
      ////

      // generate tokens
      const tokens = await generateTokens(user.id, client_id)

      // respond with '201 Created' since it is a creation
      res.status(201)

      return { user_id: user.id, client_id, tokens }
    },

    // Login with password
    'POST /login/password': async function(req) {
      let { body: { client_id, id, email, password, role } } = req

      if (!password || typeof password != 'string' || !password.trim()) {
        throw new HttpError(400, 'Empty password')
      } else {
        password = password.trim()
      }

      if (!client_id) {
        client_id = uuid()
      }

      if (role && roles.indexOf(role) === -1) {
        throw new HttpError(400, 'Invalid role. If set, it should equal one of these: ' + roles.join(', ') )
      } else {
        role = null
      }

      try {
        const user = await identifyUser({ id, email })

        if (!user) {
          throw null
        }

        const auth = await Auth.findOne({
          user_id: user.id,
          type: 'password',
          expires_at: null
        })

        if (!auth) {
          throw null
        }

        if (!await authenticatePassword(password, auth.secret)) {
          throw null
        }

        const tokens = await generateTokens(user.id, client_id)

        return { user_id: user.id, client_id, tokens }
      } catch (err) {
        throw new HttpError(403, 'Invalid email/password combination')
      }
    },

    'POST /change/password': async function(req) {
      let { body: { client_id, id, email, password, new_password } } = req

      if (!password || typeof password != 'string' || !password.trim()) {
        throw new HttpError(400, 'Empty password')
      } else {
        password = password.trim()
      }

      if (!new_password || typeof new_password != 'string' || !new_password.trim()) {
        throw new HttpError(400, 'Empty new_password')
      } else {
        new_password = new_password.trim()
      }

      if (!client_id) {
        client_id = uuid()
      }

      const user = await identifyUser({ id, email })
      let passwordAuth

      try {
        if (!user) {
          throw null
        }

        passwordAuth = await Auth.findOne({
          user_id: user.id,
          type: 'password',
          expires_at: null
        })

        // User could not yet have a password
        // So lets only throw an error if it has one
        if (passwordAuth) {
          if (!await authenticatePassword(password, passwordAuth.secret)) {
            throw null
          }
        }

      } catch (err) {
        throw new HttpError(403, 'Invalid email/password combination')
      }

      // Til here we already have the user's account authenticated,
      // so lets change it's password

      // expire old password auth if the user has one
      if ( passwordAuth ) {
        passwordAuth.expires_at = new Date()
        await passwordAuth.save()
      }

      // we should create a new password
      const newAuthPassword = new Auth({
        type: 'password',
        user_id: user.id,
        secret: hashPassword(new_password),
        expires_at: null
      })
      await newAuthPassword.save()

      if (!ENV_TEST) {
        const email = authPasswordChangedEmail({ user, newAuthPassword })
        await sendMailSMTP(email)
      }

      // everything went well, we dont need to return a payload
      return null
    },

    // recover methods
    'POST /recover/send/email': async function(req) {
      const { body: { id, email } } = req
      const user = await identifyUser({ id, email })

      if (!user) {
        throw new HttpError(400, 'Unable to identify user')
      }

      // Generate token
      // I've opted-in to use current uTimestamp as a variable limit and use
      // random to select a value between 0 and the retrieved limit.
      // Then, the integer will be converted into a base32 string which will be
      // substringed into a 6 chars one. This provides a secure and efficient
      // way to retrieve a token that matches the app needs.
      const token = (+(Math.random() * new Date() * 1000))
        .toString(32)
        .substr(-6)

      // Now let's save the token as an auth method that will expire in 1 hour
      const recoverAuth = new Auth({
        type: 'recover-token',
        secret: token,
        expires_at: new Date() + AUTH_RECOVER_TOKEN_TTL,
        user_id: user.id
      })

      // try to save it
      try {
        await recoverAuth.save()
      } catch (caught_err) {
        throw new HttpError(
          500,
          'Error while creating auth recover token',
          caught_err
        )
      }

      if (!ENV_TEST) {
        const email = authRecoveryEmail({ user, recoverAuth })
        await sendMailSMTP(email)
      }

      return null
    },
    'POST /recover/challenge': async function(req) {
      let { body: { id, email, token, new_password } } = req
      const user = await identifyUser({ id, email })

      if (!user) {
        throw new HttpError(400, 'Unable to identify user')
      }

      if (!new_password || typeof new_password != 'string' || !new_password.trim()) {
        throw new HttpError(400, 'Empty new_password')
      } else {
        new_password = new_password.trim()
      }

      // Grab auth
      const recoverAuth = await Auth.findOne({
        type: 'recover-token',
        user_id: user.id,
        secret: token
      })

      if (!recoverAuth) {
        throw new HttpError(403, 'Invalid recover token')
      }

      // Check if it has already expired
      if (recoverAuth.exipres_at <= new Date()) {
        throw new HttpError(403, 'Recover token has expired')
      }

      // Seems it is a valid token...
      // Lets invalidate the current user's password
      try {
        const passwordAuth = await Auth.findOne({
          type: 'password',
          user_id: user.id,
          expires_at: null
        })

        // User may not have a password set yet, so...
        if (passwordAuth) {
          passwordAuth.expires_at = new Date()
          await passwordAuth.save()
        }

        // we should create a new password
        const newAuthPassword = new Auth({
          type: 'password',
          user_id: user.id,
          secret: hashPassword(new_password),
          expires_at: null
        })
        await newAuthPassword.save()

        // And finaly we should invalidate the auth recover token
        recoverAuth.expires_at = new Date()
        await recoverAuth.save()
      } catch (caught_err) {
        throw new HttpError(
          500,
          'Error while recovering your password',
          caught_err
        )
      }

      if (!ENV_TEST) {
        const email = authPasswordChangedEmail({ user, recoverAuth, newAuthPassword })
        await sendMailSMTP(email)
      }

      // everything went well, we dont need to return a payload
      return null
    },

    'GET /': false,
    'GET /:id([0-9a-fA-F]{24})': false,
    'PUT /:id([0-9a-fA-F]{24})': false,
    'DELETE /:id([0-9a-fA-F]{24})': false
  }
})

async function identifyUser({ email, id }) {
  let user = null

  if (id) {
    try {
      user = await User.findById(id)
    } catch (e) {}
  }

  if (!user && email) {
    try {
      user = await User.findByEmail(email)
    } catch (e) {}
  }

  return user
}

export default AuthService
