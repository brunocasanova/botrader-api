// NOTICE: THIS IS AN EXAMPLE SERVICE

import { Trade } from '../models'
import Service from '../utils/service-builder'

export const Trades = new Service({
  model: Trade,
  endpoint: '/trades',
})

export default Trades
