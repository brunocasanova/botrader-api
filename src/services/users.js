import { User } from '../models'
import Service from '../utils/service-builder'
import parseToken from '../middlewares/parse-token'
import disallow from '../middlewares/disallow'
import secureFields from '../middlewares/secure-fields'
import HttpError from '../utils/http-error'


async function disableAccessToOtherUsers(req, id, obj) {
  if (req.user_id == obj._id) {
    return
  }

  // else, disallow unauthenticated or regular users
  await disallow({
    unauthenticated: true,
    authenticatedRole: 'user'
  })(req)
}

export const Users = new Service({
  model: User,
  endpoint: '/users',

  private_fields: [
    'ssn'
  ],

  middlewares: [
    parseToken,
    secureFields(['external_ids', 'ssn', 'emails', 'role'])
  ],

  hooks: {
    // Dont allow anyone (except admins) to edit user data
    'before list read': [
      disallow({
        unauthenticated: true,
        authenticatedRole: 'user'
      })
    ],
    'after get read': [ disableAccessToOtherUsers ],
    'before update write': [ disableAccessToOtherUsers ]
  },

  custom: {

    // Proxy /me request into /authenticated_id
    'GET /me': function (req) {
      req.params.id = req.user_id
      return this['GET /:id([0-9a-fA-F]{24})'].apply(this, arguments)
    },
    'PUT /me': function (req) {
      req.params.id = req.user_id
      return this['PUT /:id([0-9a-fA-F]{24})'].apply(this, arguments)
    },


    // Dont allow anyone to create, list or delete users
    'POST /': false,
    'DELETE /:id([0-9a-fA-F]{24})': false
  }
})

export default Users
