import Agenda from 'agenda'
import { SCHEDULER_DB_URI } from './config'

export const agenda = new Agenda({ db: { address: SCHEDULER_DB_URI } })
export default agenda
