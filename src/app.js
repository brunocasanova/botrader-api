import express from 'express'
import bodyParser from 'body-parser'
import helmet from 'helmet'
import morgan from 'morgan'
import responseHandler from './utils/response-handler'
import errorHandler from './utils/error-handler'
import nextAsync from './utils/next-async'
import { API_MORGAN, API_HELMET, API_CORS } from './config'

const app = express()
export default app

app.use(bodyParser.json())

if (API_MORGAN) app.use(morgan('tiny'))
if (API_HELMET) app.use(helmet())

import cors from 'cors'
if (API_CORS) {
  app.use(cors(typeof API_CORS == 'object' ? API_CORS : {}))
}

// load up middleware
import * as middlewares from './middlewares'
for (let middleware in middlewares) {
  if (typeof middleware == 'function') {
    app.use(nextAsync(middleware))
  }
}

// load up services
import * as services from './services'
for (let name in services) {
  const service = services[name]
  if (service && service.setup) {
    service.setup(app)
  }
}

// Handlers
app.use(responseHandler)
app.use(errorHandler)
