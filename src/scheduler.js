import logger from './logger'
import agenda from './agenda'

import * as Tasks from './tasks'

agenda.once('ready', () => {

  for ( let identifier in Tasks ) {
    let Task = Tasks[identifier]
    if ( ! Task ) continue

    identifier = Tasks.id || identifier

    logger.info( `Setting up task: "${identifier}"` )

    // Set up schedule task
    agenda.define(identifier, Task.options || {}, async ( job, done ) => {
      logger.info( `Executing task "${identifier}"` )

      let res
      try{
        res = await Task.run( job )
      } catch(err) {
        logger.error( `Task "${identifier}" has exited with error - ${err.message}` )
        return done(err)
      }

      logger.info( `Task "${identifier}" has been completed`, res )
      done()
    })

    if ( Task.cron ) {
      logger.info( `Schedulng task "${identifier}" to every ${Task.cron}` )
      agenda.every(Task.cron, identifier)
    }
  }

  // Everything is set! Lets start
  agenda.start()
})