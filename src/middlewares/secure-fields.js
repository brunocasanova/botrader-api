import { User } from '../models'
import HttpError from '../utils/http-error'
import { JWT_AUTHORIZATION_REGEXP, decodeToken } from '../utils/json-web-token'

const methods = [ 'POST', 'PUT' ]

function secureFields ( fields = [], roles = [ null ] ) {
  return async ( req ) => {
    // const user = await req.getUser()

    if ( methods.indexOf(req.method) === -1 ) {
      return
    }

    // Dont allow changes to critical data
    if (roles.indexOf( req.user_role ) !== -1) {
      const { body } = req

      for (let key of fields) {
        if ( typeof body[key] != 'undefined' ) {
          delete body.key
        }
      }
    }
  }
}

export default secureFields
