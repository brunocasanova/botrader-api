import { User } from '../models'
import HttpError from '../utils/http-error'
import { JWT_AUTHORIZATION_REGEXP, decodeToken } from '../utils/json-web-token'

const defaultOptions = {
  unauthenticated: false,
  authenticated: false,
  authenticatedRole: false,
}

function disallow ( options = {} ) {
  const {
    unauthenticated,
    authenticated,
    authenticatedRole,
  } = {
    ...defaultOptions,
    ...options
  }

  return async ( req ) => {

    if ( unauthenticated && ! user ) {
      throw new HttpError( 403, 'Unauthenticated users are not allowed to access this endpoint' )
    }

    if ( authenticated && user ) {
      throw new HttpError( 403, 'Authenticated users are not allowed to access this endpoint' )
    }

    if ( typeof authenticatedRole != 'undefined' && authenticatedRole === req.user_role ) {
      throw new HttpError( 403, 'This user role cannot access this endpoint' )
    }

  }
}

export default disallow
