import { User } from '../models'
import HttpError from '../utils/http-error'
import { JWT_AUTHORIZATION_REGEXP, decodeToken } from '../utils/json-web-token'

async function parseToken(req) {
  const { headers: { authorization }, query: { access_token } } = req

  // Before anything else, lets bind methods to the req object
  req.getUser = getUser.bind(req)

  let payload
  let token

  if ( typeof authorization == 'string' && authorization.match(/^JWT .*$/) ) {
    token = authorization.split(' ')[1]
  }

  if ( typeof access_token == 'string' && access_token ) {
    token = access_token
  }

  if ( ! token ) {
    throw new HttpError( 401, 'Please add access_token query variable to your request, or Authorization JWT header' )
  }

  try {
    payload = await decodeToken( token )
  } catch ( e ) {
    throw new HttpError( 403, 'Invalid token' )
  }

  req.user_id = payload.user
  req.user_role = payload.role || null
  req.client_id = payload.client
  req.auth_payload = payload
}

// auxiliar req methods

async function getUser () {
  const { user_id } = this

  if ( ! user_id ) {
    return null
  }

  const user = this._user =
    this._user ||
    (await User.findById(user_id).exec())

  return user || null
}


export default parseToken
