import * as Marketplaces from '../marketplaces'

const markets = []

for ( let Marketplace of Marketplaces ) {
  if ( Marketplace.markets ) {
    markets = [
      ...markets,
      ...Marketplace.markets
    ]
  }
}

console.log(markets)

export default markets