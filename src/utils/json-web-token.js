import jwt from 'jsonwebtoken'
// import Promise from 'bluebird'
import { JWT_SECRET, JWT_ALGORITHM, JWT_ACCESS_VALIDITY, JWT_REFRESH_VALIDITY } from '../config'
import { User } from '../models'

const options = {
  algorithm: JWT_ALGORITHM,
  expiresIn: JWT_ACCESS_VALIDITY
}

export const JWT_AUTHORIZATION_REGEXP = /^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/

export async function decodeToken (token) {
  return jwt.verify(token, JWT_SECRET)
}

export async function generateTokens (user, client, role = 'user') {
  const refresh = await generateRefreshToken(user, client, role)
  const access = await generateAccessToken(user, client, role)

  return {access, refresh}
}

export async function renewAccessToken (refresh, rcvdClient) {
  const { user, client, role } = jwt.verify(refresh, JWT_SECRET)

  if (client != rcvdClient) {
    throw new Error( 'different client id' )
  }

  // TODO: Verify token on user's generated tokens
  const access = await generateRefreshToken(user, client, role)

  return access
}

// private methods
async function generateAccessToken ( user, client, role = 'user') {
  // TODO: Save refresh on user's account later
  return jwt.sign({user, client}, JWT_SECRET, options)
}

async function generateRefreshToken ( user, client, role = 'user' ) {
  // TODO: Save refresh on user's account later
  return jwt.sign({user, client}, JWT_SECRET, {...options, expiresIn: JWT_REFRESH_VALIDITY})
}
