import HttpError from './http-error'
import { ENV } from '../config'

export default ( err, req, res, next ) => {
  let finalError
  if (ENV === 'development' /*|| ENV === 'test'*/) {
    console.error(err && err.stack || err && err.message || err)
  } else if (!err.code || err.code === 500) {
    // TODO: trigger some sort of reporting mechanism when the error equals to 500 (Internal Server Error)
    console.error(err && err.stack || err && err.message || err)
  }

  if ( ! ( err instanceof HttpError ) ) {
    finalError = new HttpError( 500, 'Internal Server Error', err )
  } else {
    finalError = err
  }

  res.statusMessage = finalError.message || res.statusMessage
  res.status(finalError.code)
  res.send({
    message: finalError.message
  })
}
