import HttpError from './http-error'

export default ( req, res /*, next*/ ) => {

  if( typeof res.payload != 'undefined' ) {
    const response = {
      message: 'OK',
      payload: res.payload,
      ...res.complement
    }

    const stringified = JSON.stringify( response, stringifyReplacer )

    // Add header manually
    res.header('Content-Type', 'application/json')

    return res.send( stringified )
  }

  throw new HttpError(404, 'Endpoint not found')
}

function stringifyReplacer (key, value) {
  return (
    value === Infinity && 'Infinity' ||
    value === -Infinity && '-Infinity' ||
    value === NaN && 'NaN' ||
    value
  )
}
