import HttpError from '../utils/http-error'
import { Router } from 'express'
import nextAsync from '../utils/next-async'

export const METHODS = ['all', 'options', 'get', 'put', 'post', 'delete']
export const ACTIONS = {
  list: 'GET /',
  create: 'POST /',
  read: 'GET /:id([0-9a-fA-F]{24})',
  update: 'PUT /:id([0-9a-fA-F]{24})',
  delete: 'DELETE /:id([0-9a-fA-F]{24})',
}

const DEFAULT_OPTIONS = {
  model: undefined,
  private_fields: [],
  secured_fields: [ '_id', '__v' ],
  search_fields: [],
  sort_fields: [],
  actions: Object.keys(ACTIONS)
}

export default class Service {

  constructor(options) {
    options = this.options = { ...DEFAULT_OPTIONS, ...options }

    const { model, endpoint, actions } = options

    if (!endpoint) {
      throw new Error('An endpoint is needed')
    }

    if (model) {
      this.model = model
    }

    // rebind default actions based on the array
    if (Array.isArray( actions ) ) {
      for ( let action of actions ) {
        const url = ACTIONS[action]

        if ( url ) {
          this[ url ] = this[action].bind(this)
        }
      }
    }

    if (typeof options.custom == 'object') {
      for (let name in options.custom) {
        this[name] = options.custom[name]
      }
    }

    this.hooks = (typeof options.hooks == 'object' && options.hooks) || {}
    this.middlewares =
      (Array.isArray(options.middlewares) && options.middlewares) || []
  }

  // Begin of default methods

  async list (req, res) {
    const complement = res.complement = {}
    const { options: { private_fields, search_fields, sort_fields }, model, hook } = this

    // sorting
    // if ( req.query.sort && Array.isArray(sort_fields) && sort_fields.length ) {
    //   req.query.sort.split()

    //   // check if sort field
    // }

    // search
    const search =
      typeof req.query.search == 'string' &&
        req.query.search
          .replace(/\W+/g, ' ').trim()||
      null

    const page = (+req.query.page > 0 && +req.query.page) || 0
    const limit = (+req.query.limit > 0 && +req.query.limit) || 5
    const offset = page * limit

    let query = model
      .find()
      .skip(offset).limit(limit)

    if ( search && search_fields && search_fields.length ) {
      // query
      //   .where({ $text: { $search: search +'*' } })
      //   .select({ _score: { $meta: 'textScore' } })
      //   .sort({ _score: { $meta: 'textScore' } })

      const searchWhere = {}

      for ( let field of search_fields ) {
        searchWhere[field] = {
          $regex: search,
          $options: 'ims'
        }
      }

      query.where(searchWhere)

      complement.search = search
    }

    // remove __v field
    query.select('-__v')

    await hook('before list read', req, query)

    const objs = await query.exec()

    const path = req.baseUrl

    // add other relevant info
    complement.pagination = {
      page,
      limit,
      prev: (page > 0 && `${path}?limit=${limit}&page=${page - 1}`) || null,
      next:
        (objs.length == limit && `${path}?limit=${limit}&page=${page + 1}`) ||
        null
    }

    await hook('after list read', req, objs, complement)

    // hide private fields
    if ( private_fields.length ) {
      for ( let obj of objs ) {
        removeFieldsFromObject(obj, private_fields)
      }
    }

    return objs
  }

  async create (req, res) {
    const { body } = req
    const { options: { private_fields, secured_fields }, model, hook } = this

    // remove secured fields
    if ( secured_fields.length ) {
      removeFieldsFromObject(body, secured_fields)
    }

    await hook('before create write', req, body)
    const obj = await model.create(body)
    await hook('after create write', req, obj, body)

    // hide private fields
    if ( private_fields.length ) {
      removeFieldsFromObject(obj, private_fields)
    }

    res.status(201)
    return obj
  }

  async read (req) {
    const { params: { id } } = req
    const { options: { private_fields }, model, hook } = this

    const query = model.findById(id)

    // remove __v field
    query.select('-__v')

    await hook('before get read', req, id, query)
    const obj = await query.exec()
    await hook('after get read', req, id, obj)

    if (!obj) {
      throw new HttpError(404, 'Not found')
    }

    // hide private fields
    if ( private_fields.length ) {
      removeFieldsFromObject(obj, private_fields)
    }

    return obj
  }

  async update (req) {
    const { params: { id }, body } = req
    const { options: { private_fields, secured_fields }, model, hook } = this
    let obj

    if (typeof body != 'object' || Object.keys(body).length === 0) {
      throw new HttpError(400, 'Bad Request')
    }

    // remove secured fields
    if ( secured_fields.length ) {
      removeFieldsFromObject(body, secured_fields)
    }

    const query = model.findById(id)
    await hook('before update read', req, id, query)
    obj = await query.exec()
    await hook('after update read', req, obj)

    if (!obj) {
      throw new HttpError(404, 'Not found')
    }

    await hook(
      'before update write',
      req,
      id,
      obj,
      body
    ) /* req, id, current, changes */
    obj = await obj.update(body)
    await hook(
      'after update write',
      req,
      obj,
      body
    ) /* req, id, current, changed */

    // hide private fields
    if ( private_fields.length ) {
      removeFieldsFromObject(obj, private_fields)
    }

    return obj
  }

  async delete (req) {
    const { params: { id } } = req
    const { model, hook } = this
    let obj

    await hook('before delete read', req, id)
    obj = await model.get(id)
    await hook('after delete read', req, obj)

    if (!obj) {
      throw new HttpError(404, 'Not found')
    }

    await hook('before delete write', req)
    await obj.remove()
    await hook('after delete write', req)
  }

  // End of default methods

  hook = async (hk, ...args) => {
    const { options: { endpoint }, hooks } = this

    if (hooks && typeof hooks[hk] == 'function') {
      await hooks[hk].apply(this, args)
      return
    }

    if (hooks && Array.isArray(hooks[hk])) {
      for (let hook of hooks[hk]) {
        if (typeof hook == 'function') {
          await hook.apply(this, args)
        } else {
          console.warn(
            `[${endpoint}@${hk}] - You have a non-function method on hooks array`
          )
        }
      }
      return
    }

    if (hooks[hk]) {
      console.warn(
        `[${endpoint}@${hk}] - You have a non-function/array value set on hooks`
      )
    }
  }

  setup(app) {
    const { options: { endpoint }, middlewares } = this

    if (this.router) {
      throw new Error(`[${endpoint}] - Service was already configured`)
    }

    const router = (this.router = new Router())

    if (middlewares) {
      for (let middleware of middlewares) {
        if (typeof middleware == 'function') {
          console.log(
            `[${endpoint}] - setting up middleware: ${middleware.name ||
              'anonymous'}`
          )
          router.use(nextAsync(middleware))
        } else {
          throw new Error(
            `[${endpoint}] - You have placed a middleware that is not a function`
          )
        }
      }
    }

    for (let methodName in this) {
      if (typeof this[methodName] != 'function') {
        continue
      }

      const methodAPI = methodName.split(' ')

      if (methodAPI.length < 2) {
        continue
      }

      const method = methodAPI.splice(0, 1)[0].toLowerCase()

      if (!METHODS.includes(method)) {
        continue
      }

      const path = methodAPI.splice(0, 1)[0]

      console.log(`[${endpoint}] - setting up: ${method} ${path}`)
      router[method].call(router, path, nextAsync(this[methodName].bind(this)))
    }

    app.use(endpoint, router)

    console.log(`[${endpoint}] - setup completed`)
  }
}

function removeFieldsFromObject ( obj, fields = [] ) {
  if ( fields.length === 0 ) {
    return
  }

  for ( let field of fields ) {
    obj[field] = undefined
    delete obj[field]
  }

  return obj
}
