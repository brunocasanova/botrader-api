import mongoose from 'mongoose'
import { NODE_ENV, DB_URI } from './config'

const config = {
  autoIndex: NODE_ENV !== 'production'
}


// Use native Promise on mongoose
mongoose.Promise = Promise

export const Types = mongoose.Schema.Types
export const Schema = mongoose.Schema

export default mongoose.connect(DB_URI, { config, useMongoClient: true })
export const connection = mongoose.connection



export function createModel(name, schema, options = {}) {
  const Schema = new mongoose.Schema(schema, {
    toObject: {
      transform: (doc, obj, game) => {
        // remove __v from objects
        delete obj.__v
        return obj
      }
    }
  })

  if ( typeof options == 'object' ) {

    // handle hooks
    if( options.hooks ) {
      for ( let when of [ 'pre', 'post' ] ) {
        for ( let what of [ 'update', 'save' ] ) {
          const name = when + ' ' + what

          if ( typeof options.hooks[name] == 'function' ) {
            Schema[when](what, asyncHookifier( options.hooks[name] ) )
          }
        }
      }
    }

    if( Array.isArray( options.plugins ) ) {
      for ( let plugin of options.plugins ) {
        Schema.plugin( plugin )
      }
    }

  }

  return {
    Schema,
    [name]: mongoose.model(name, Schema)
  }
}


const asyncHookifier = (method) => async function (next) {
  let next_called = false
  let data

  try {
    data = await method.call(this, d => {
      next_called = true

      data = d
      next()
    })
  } catch (err) {
    next(err)
    return
  }

  if (!next_called) {
    next(data)
  }
}
