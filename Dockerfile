FROM cusspvz/node:7.6.0

# Allow environment exchange with build-args
ARG NODE_ENV=production

ADD package.json /app

# Build and install production modules
RUN apk --update upgrade && \
    APK_NEEDS="make gcc g++ python linux-headers"; \
    apk add $APK_NEEDS && \
    npm install --production && \
    apk del $APK_NEEDS && \
    rm -fR /var/cache/apk/* ~/.npmrc

# Add entire context
ADD . /app

# Whenever the image is started without a command, use start into entrypoint
# We are not calling here `npm run start` because entrypoint already handles it.
EXPOSE 8080
CMD [ "start" ]
